"use strict";
// 1. Задача
function numbers(num1, num2) {
  const result = num1 % num2;
  return result;
}
console.log(numbers(10, 3));

// 2. Задача
let firstNumber = prompt("Введіть перше число!: ");
let operators = prompt("Введіть оператор!: ");
let secondNumber = prompt("Введіть друге число!: ");

function calc(firstNumber, operators, secondNumber) {
  let num1 = parseInt(firstNumber);
  let num2 = parseInt(secondNumber);

  if (
    operators !== "+" &&
    operators !== "-" &&
    operators !== "*" &&
    operators !== "/"
  ) {
    alert("Такої операції не існує");
  }
  switch (operators) {
    case "+":
      console.log("Результат при додавані:", num1 + num2);
      break;
    case "-":
      console.log("Результат при відніманні:", num1 - num2);
      break;
    case "*":
      console.log("Результат при множенні:", num1 * num2);
      break;
    case "/":
      console.log("Результат при діленні:", num1 / num2);
      break;
  }
}
calc(firstNumber, operators, secondNumber);
// не зміг зробити щоб воно перезапитувало через do while поки юзер не введе "намбер", робив аби як і воно ламало ввесь інший код, щось вайли для мене важчі ніж фор

// 3. Задача
let userNumber = parseInt(prompt("Введіть число: "));
function factorial(n) {
  if (userNumber === 0 || userNumber === 1) return 1;
  for (let i = userNumber - 1; i >= 1; i--) {
    userNumber *= i;
  }
  return userNumber;
}
console.log(factorial());
